//
//  NewCollectionVC.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/27/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse

class EditCollectionVC: Commons, UIPickerViewDelegate {
    
    @IBOutlet weak var nameCollection: CustomTextField!
    @IBOutlet weak var categoriesPickerView: UIPickerView!
    var listCategories: [CategorieDto] = []
    var collectionSelected: CollectionDto! = CollectionDto()
    var colecao:PFObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateLisCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func populateLisCategories() {
        var categoriesTeste = CategorieDto()
        categoriesTeste.id = 1
        categoriesTeste.name = "Books"
        listCategories.append(categoriesTeste)
        
        categoriesTeste = CategorieDto()
        categoriesTeste.id = 2
        categoriesTeste.name = "Movies"
        listCategories.append(categoriesTeste)
        
        categoriesTeste = CategorieDto()
        categoriesTeste.id = 3
        categoriesTeste.name = "HQs"
        listCategories.append(categoriesTeste)
        
        categoriesTeste = CategorieDto()
        categoriesTeste.id = 4
        categoriesTeste.name = "Magazines"
        listCategories.append(categoriesTeste)
    }
    
    @IBAction func saveCollection(sender: AnyObject) {
        
        if !nameCollection.text!.isEmpty {
            collectionSelected.name = nameCollection.text
            collectionSelected.categorie = listCategories[categoriesPickerView.selectedRowInComponent(0)]
            
            let novaColecao = PFObject(className: "Colecao")
            
            novaColecao["nome"] = nameCollection.text
            novaColecao["User"] = PFUser.currentUser()
            
            novaColecao.saveInBackgroundWithBlock { (sucesso, error) -> Void in
                if(error == nil){
                    print("Salvou a colecao: \(self.nameCollection.text)")
                    self.nameCollection.text = ""
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }
            
        } else {
            alertRequiredFields()
        }
        
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listCategories.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listCategories[row].name
    }
}
