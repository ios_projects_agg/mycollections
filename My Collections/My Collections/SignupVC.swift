//
//  SignupVC.swift
//  My Collections
//
//  Created by Arthur Givigir on 10/4/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse

class SignupVC: Commons {
    
    @IBOutlet weak var name: CustomTextField!
    @IBOutlet weak var email: CustomTextField!
    @IBOutlet weak var confirmEmail: CustomTextField!
    @IBOutlet weak var password: CustomTextField!
    @IBOutlet weak var confirmPassword: CustomTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem(title: "Login", style: .Plain, target: self, action: "backToLoginFromBarItem:")
        self.navigationItem.leftBarButtonItem = backButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func backToLoginFromBarItem(sender: UIBarButtonItem) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func signUp(sender: AnyObject) {
        
        if name.text!.isEmpty || email.text!.isEmpty || confirmEmail.text!.isEmpty || password.text!.isEmpty || confirmPassword.text!.isEmpty {
            alertRequiredFields()
        
        } else if email.text != confirmEmail.text {
            alertWithMessage("Ops... Check de Confirm E-mail field!")
            
        } else if password.text != confirmPassword.text {
            alertWithMessage("Ops... Check de Confirm Password field!")
            
        } else {
            
            let novoUsuario = PFUser()
            
            novoUsuario.email = email.text
            novoUsuario.username = name.text
            novoUsuario.password = password.text
            
            novoUsuario.signUpInBackgroundWithBlock { (sucesso, erro) -> Void in
                if(sucesso){
                    print("Logou com sucesso o usuario \(novoUsuario.username)")
                    self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }

    }
    
}
