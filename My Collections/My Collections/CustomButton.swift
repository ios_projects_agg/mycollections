//
//  CustomUIButton.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/23/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class CustomUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        super.layer.borderWidth = 1.0
        super.layer.cornerRadius = 5.0
        super.layer.borderColor = UIColor.whiteColor().CGColor
        super.layer.backgroundColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.2).CGColor
    }
    
}
