//
//  Commons.swift
//  My Collections
//
//  Created by Arthur Givigir on 10/4/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class Commons: UIViewController, UITextViewDelegate {
    
    func alertRequiredFields() {
        let alert = UIAlertController(title: "Something is Wrong!", message: "Ops... Check the required fields...", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertWithMessage(message: String) {
        let alert = UIAlertController(title: "Something is Wrong!", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func hideKeyboard(sender: AnyObject) {
        self.view.endEditing(true)
        print("tocou")
    }
}
