//
//  ListItensVC.swift
//  My Collections
//
//  Created by Arthur Givigir on 10/4/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ListItensVC: Commons {

    @IBOutlet weak var itensTableView: UITableView!
    var listItens = NSArray()
    var colecao:PFObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        // Carregando as colecoes do usuario logado
        let query = PFQuery(className: "Item")
        query.whereKey("colecao", equalTo: colecao!)
        
        query.findObjectsInBackgroundWithBlock({ (itens, error) -> Void in
            if(error == nil){
                print("Itens retornados: \(itens?.count)")
                self.listItens = itens!
                self.itensTableView.reloadData()
            }else{
                print("Erro na query: \(error?.description)")
            }
        })
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:ImageParseCell = (tableView.dequeueReusableCellWithIdentifier("ParseCell", forIndexPath: indexPath) as? ImageParseCell)!
        
        let itemCollection:AnyObject = listItens[indexPath.row]
        cell.itemLabel.text = (itemCollection["nome"] as? String)
        
        if let file = itemCollection["imagem"] as? PFFile {
            cell.itemImage.file = file
            cell.itemImage.loadInBackground()
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItens.count
    }
    
    func backToListCollections(sender: UIBarButtonItem) {
        self.navigationController!.popToRootViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let itemViewController:EditItemVC = segue.destinationViewController as! EditItemVC
        
        if(segue.identifier == "item"){
            let index = itensTableView.indexPathForSelectedRow!.row
            let item = listItens[index] as! PFObject
            itemViewController.item = item
        }
        
        itemViewController.colecao = colecao!
    }
}
