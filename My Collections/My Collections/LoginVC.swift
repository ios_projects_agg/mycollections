//
//  ViewController.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/21/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse

class LoginVC: Commons {
    
    @IBOutlet weak var email: CustomTextField!
    @IBOutlet weak var password: CustomTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func login(sender: AnyObject) {
        
        if (email.text!.isEmpty || password.text!.isEmpty) {
            alertRequiredFields()
            
        } else {
            
            PFUser.logInWithUsernameInBackground(email.text!, password: password.text!){ ( user, error) -> Void in
                if(user != nil) {
                    print("usuario logou!")
                    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
                    let vc : UIViewController = storyboard.instantiateViewControllerWithIdentifier("firstNavigationController") as UIViewController;
                    self.presentViewController(vc, animated: true, completion: nil);

                }else{
                    
                    self.email.text = ""
                    self.password.text = ""
                    
                    self.email.becomeFirstResponder()
                    self.alertWithMessage("Ops... Verify your login and password...")
                }
            }
        }
        
    }
}

