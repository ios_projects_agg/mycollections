//
//  CustomNavigationController.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/27/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.navigationBar.barTintColor = UIColor(red:0.40, green:0.67, blue:0.24, alpha:0.1)
        super.navigationBar.translucent = true
        super.navigationBar.barStyle = UIBarStyle.BlackTranslucent
        super.navigationBar.tintColor = UIColor.whiteColor()
    }
}
