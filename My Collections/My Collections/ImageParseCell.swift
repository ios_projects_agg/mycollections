//
//  ImageParseCell.swift
//  My Collections
//
//  Created by Igor Silva on 08/10/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import ParseUI

class ImageParseCell: UITableViewCell {

    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemImage: PFImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
