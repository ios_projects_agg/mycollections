//
//  CollectionDto.swift
//  My Collections
//
//  Created by Arthur Givigir on 10/4/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class CollectionDto: NSObject {
    
    @IBOutlet var id: NSNumber! = 0.0
    @IBOutlet var categorie: CategorieDto!
    @IBOutlet var name: String!

}
