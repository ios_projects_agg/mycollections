//
//  EditItemVC.swift
//  My Collections
//
//  Created by Arthur Givigir on 10/4/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse

class EditItemVC: Commons, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var itemName: CustomTextField!
    @IBOutlet weak var itemDescription: UITextView!
    @IBOutlet weak var itemHave: UISegmentedControl!
    @IBOutlet weak var imgItem: UIImageView!
    var colecao:PFObject?
    var item:PFObject?

    override func viewDidLoad() {
        super.viewDidLoad()
        if(item != nil){
            itemName.text = item!["nome"]  as? String
            itemDescription.text = item!["descricao"] as! String
            itemHave.selectedSegmentIndex = (item!["possui"] as! Bool) == true ? 0 : 1
            
            if let file:PFFile = item!["imagem"] as? PFFile {
                file.getDataInBackgroundWithBlock({ (data, error) -> Void in
                    if(error == nil){
                        self.imgItem.image = UIImage(data:data!)
                    }
                })
            }
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imgItem.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func addPhoto(sender: AnyObject) {
        let pickerVC = UIImagePickerController()
        pickerVC.delegate = self
        pickerVC.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(pickerVC, animated: true, completion: nil)
    }
    
    @IBAction func saveItem(sender: AnyObject) {
        if itemName.text!.isEmpty {
            alertWithMessage("Ops... Item name is required!")
        } else {
            
            if(item == nil){
                item = PFObject(className: "Item")
            }
            
            if(self.imgItem.image != nil){
                let data = UIImageJPEGRepresentation(self.imgItem.image!, 0.8)
                let file = PFFile(name: "imagem.jpg", data:data!)
                item!["imagem"] = file
            }
            
            item!["nome"] = itemName.text
            item!["descricao"] = itemDescription.text
            item!["colecao"] = colecao
            item!["possui"] = itemHave.selectedSegmentIndex == 0 ? true : false
            
            
            item?.saveInBackgroundWithBlock({ (sucesso, error) -> Void in
                if(sucesso){
                    print("Item salvo!")
                    self.itemDescription.text = ""
                    self.itemName.text = ""
                    self.imgItem.image = nil
                    self.itemHave.selectedSegmentIndex = 0
                    self.navigationController?.popViewControllerAnimated(true)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
