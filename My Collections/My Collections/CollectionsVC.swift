//
//  ColectionsVC.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/27/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit
import Parse

class CollectionsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableCollections: UITableView!
    
    var listCollections = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem(title: "Logoff", style: .Plain, target: self, action: "logoff:")
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        if PFUser.currentUser() == nil {
            self.performSegueWithIdentifier("login", sender: nil)
        }else{
            // Carregando as colecoes do usuario logado
            let query = PFQuery(className: "Colecao")
            
            query.whereKey("User", equalTo: PFUser.currentUser()!)
            
            query.findObjectsInBackgroundWithBlock({ (colecoes, error) -> Void in
                if(error == nil){
                    print("Colecoes retornadas: \(colecoes?.count)")
                    self.listCollections = colecoes!
                    self.tableCollections.reloadData()
                }else{
                    print("Erro na query: \(error?.description)")
                }
            })
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ParseCell", forIndexPath: indexPath)
        
        let itemCollection:AnyObject = listCollections[indexPath.row]
        cell.textLabel?.text = (itemCollection["nome"] as? String)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCollections.count
    }
    
    func logoff(sender: UIBarButtonItem) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        let vc : UIViewController = storyboard.instantiateViewControllerWithIdentifier("login") as UIViewController;
        self.presentViewController(vc, animated: true, completion: nil);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "listItens"{
            let itensViewController:ListItensVC = segue.destinationViewController as! ListItensVC
            let index = tableCollections.indexPathForSelectedRow!.row
            let colecao = listCollections[index] as! PFObject
            itensViewController.colecao = colecao
        }
    }
}
