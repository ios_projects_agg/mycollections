//
//  CustomTextField.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/21/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        super.borderStyle = UITextBorderStyle.RoundedRect

        let paddingView = UIView(frame: CGRectMake(0, 0, 15, super.frame.height))
        super.leftView = paddingView
        super.leftViewMode = UITextFieldViewMode.Always
        super.borderStyle = UITextBorderStyle.RoundedRect
        super.alpha = 0.8
        
    }

}
