//
//  CustomButtonWithBg.swift
//  My Collections
//
//  Created by Arthur Givigir on 9/23/15.
//  Copyright © 2015 Arthur Givigir. All rights reserved.
//

import UIKit

class CustomButtonWithBg: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        super.layer.borderWidth = 1.0
        super.layer.cornerRadius = 5.0
        super.setTitleColor(UIColor(red:0.40, green:0.67, blue:0.24, alpha:1.0), forState: UIControlState.Normal)
        
        super.layer.backgroundColor = UIColor.whiteColor().CGColor
        super.layer.borderColor = UIColor.whiteColor().CGColor

    }


}
